const express = require("express");
const router = express.Router();

const { Thread } = require("../model/models");

router.post("/create", (req, res) => {
	const reqBody = req.body;

	Thread.create({
		title: reqBody.title,
		content: reqBody.content,
		AccountUsername: reqBody.username,
	})
		.then(() => {
			res.status(201).json({
				code: 201,
				message: "Thread created",
				data: null,
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

router.get("/", (req, res) => {
	let currentPage = req.query.page;
	currentPage = currentPage == undefined || currentPage < 1 ? 1 : currentPage;
	const limit = 10;
	const firstData = currentPage * limit - limit;

	Thread.findAndCountAll({
		where: {
			is_thread: true,
		},
		limit: limit,
		offset: firstData,
		order: [["createdAt", "DESC"]],
		attributes: {
			exclude: ["is_thread", "ThreadId"],
		},
	})
		.then((thread) => {
			res.status(200).json({
				code: 200,
				message: "Success",
				data: {
					threads: thread.rows,
					pagination: {
						currentPage: parseInt(currentPage),
						totalPage: Math.ceil(thread.count / limit),
					},
				},
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

router.get("/:thread_id", (req, res) => {
	const threadId = parseInt(req.params.thread_id);

	Thread.findOne({
		where: {
			id: threadId,
		},
		attributes: {
			exclude: ["is_thread", "ThreadId"],
		},
	})
		.then((thread) => {
			res.status(200).json({
				code: 200,
				message: "Success",
				data: {
					threads: thread,
				},
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

module.exports = router;
