const express = require("express");
const bcrypt = require("bcrypt");
const { Account } = require("../model/models");

const router = express.Router();

router.post("/register", (req, res) => {
	const reqBody = req.body;

	Account.create({
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10),
	})
		.then(() => {
			res.status(201).json({
				code: 201,
				message: "Account created",
				data: null,
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

router.post("/login", (req, res) => {
	const reqBody = req.body;

	Account.findOne({
		where: {
			username: reqBody.username,
		},
		attributes: ["username"],
	})
		.then((account) => {
			res.status(200).json({
				code: 200,
				message: "Login success",
				data: {
					username: account.username,
				},
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

module.exports = router;
