const express = require("express");
const { Thread } = require("../model/models");
const router = express.Router();

router.post("/create", (req, res) => {
	const reqBody = req.body;
	console.log(reqBody);

	Thread.create({
		content: reqBody.comment,
		AccountUsername: reqBody.username,
		ThreadId: reqBody.thread_id,
		is_thread: false,
	})
		.then(() => {
			res.status(201).json({
				code: 201,
				message: "Comment created",
				data: null,
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

router.get("/:thread_id", (req, res) => {
	const threadId = req.params.thread_id;

	Thread.findAll({
		where: {
			ThreadId: threadId,
		},
		attributes: {
			exclude: ["title", "is_thread", "updatedAt", "ThreadId"],
		},
	})
		.then((comment) => {
			res.status(200).json({
				code: 200,
				message: "Success get comments",
				data: {
					comments: comment,
				},
			});
		})
		.catch((err) => {
			res.status(400).json({
				code: 400,
				message: "Error occured",
				data: {
					errors: err,
				},
			});
		});
});

module.exports = router;
