const express = require("express");
const cors = require("cors");
const initialMigrate = require("./model/migrate");
require("dotenv").config();

const app = express();
app.use(express.json());
app.use(cors());

// initialMigrate();

app.use("/account", require("./routes/account"));
app.use("/thread", require("./routes/thread"));
app.use("/comment", require("./routes/comment"));

app.listen(process.env.PORT, () => {
	console.log(`app is listening on port http://localhost:${process.env.PORT}`);
});
