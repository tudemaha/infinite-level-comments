const { DataTypes } = require("sequelize");
const sequelize = require("./connection");

const Account = sequelize.define(
	"Account",
	{
		username: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true,
			primaryKey: true,
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		tableName: "accounts",
	}
);

const Thread = sequelize.define("Thread", {
	title: {
		type: DataTypes.STRING,
		allowNull: true,
		defaultValue: null,
	},
	content: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	is_thread: {
		type: DataTypes.BOOLEAN,
		allowNull: false,
		defaultValue: true,
	},
});

Account.hasMany(Thread);
Thread.belongsTo(Account);

Thread.hasMany(Thread);
Thread.belongsTo(Thread);

module.exports = { Account, Thread };
